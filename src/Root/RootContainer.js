import react from 'react';
import ConversationContainer from '../Conversation/containers/conversation-container/ConversationContainer';
import ConversationService from '../Conversation/services/ConversationService';
import UserContainer from '../User/containers/UserContainer';
import UserService from '../User/services/UserService';
import { Link } from 'react-router-dom';
import ConversationForm from '../Conversation/components/conversation-form/ConversationForm';
import './RootContainer.css';

class RootContainer extends react.Component {

    constructor(props) {
        super(props);

        this.state = {
            users: [{_id: null, username: ''}],
            defaultUser: {_id: null, username: ''},
            selectedUser: null,
            conversation: null,
            openModal: false,
            conversations: [],
            newConversation: {createdBy: '', name: '', members: []}
        }
    }

    async componentDidMount() {
        const response = await UserService.getAll();
        if(response) {
            this.setState({users: response.data});
            this.setState({defaultUser: response.data[0]});
        }
        this.loadAllConversation((this.state.defaultUser._id || this.state.selectedUser._id));
    
       this.refreshConversation();

        setInterval(async () => {
           this.state.conversation && await this.refreshMessage(this.state.conversation?._id);
        },5000);
    }

    loadAllConversation(id) {
        UserService.getUserConversation(id || this.state.defaultUser._id).then(res => {
            this.setState({conversations: res.data});
        })
    }

    refreshConversation() {
        setInterval(() => {
            if (this.state.selectedUser !== null) {
                this.loadAllConversation(this.state.selectedUser._id);
            } else {
                this.loadAllConversation(this.state.defaultUser._id);
            }
       }, 5000);
    }

    render() {
        return (
            <div style = {{display: 'flex', flexDirection: 'column', width:'100%', height:'100%', userSelect: 'none'}}>
                <div style={{opacity: this.state.openModal ? '0.3' : '1', pointerEvents: this.state.openModal ? 'none' : 'unset'}}>
                        
                    <UserContainer users = {this.state.users} onUserSelect = {this.onUserSelect} hideDetail = {this.onHideDetail}/>
                    <h4 style={{color: "green"}}>Connecté en tant que {this.state.selectedUser ? this.state.selectedUser.username : this.state.defaultUser.username}</h4>
                    <ConversationContainer conversations = {this.state.conversations} openModal = {this.onOpenModal} conversation = {this.state.conversation} onCloseConversation = {this.onCloseConversation} onAddMessage = {this.onAddMessage} onLoadDetail = {this.onLoadDetail}/>  
                    <Link to='/list' className="link">Voir toutes les conversations</Link>    
                </div>
            
                {
                    this.state.openModal && 
                    <div className="modal">
                        <ConversationForm onSubmit = {this.onSubmit} users = {this.state.users.filter(u => u._id !== (this.state.selectedUser?._id || this.state.defaultUser?._id))} onCloseModal = {this.onCloseModal}/>
                    </div>
                }
            </div>        
        )
    }

    onUserSelect = (user) => {
        this.setState({selectedUser: user});
        this.loadAllConversation(user._id);
    }

    onHideDetail = (val) => {
        this.setState({conversation: null});
    }
    
    onAddMessage = (message) => {
        const defaultVal = this.state.defaultUser._id;
        ConversationService.addMessage(this.state.conversation._id, {from: this.state.selectedUser?._id || defaultVal, message}).then(res => {
            const conversation = res.data;
            this.setState({conversation: conversation});
        })
    }

    async refreshMessage(id) {
        const res = await ConversationService.get(id);
        this.setState({conversation: res.data});
    }

    onLoadDetail = (conversation) => {
        this.setState({conversation})
    }

    onOpenModal = (value) => {
        this.setState({openModal: value});
    }

    onCloseModal = () => {
        this.setState({openModal: false})
    }

    onCloseConversation = (id) => {
        const index = this.state.conversations.map(c => c._id).indexOf(id);
        ConversationService.closeConversation(id).then((res) => {
            const conversation = res.data;
            let conversations = [...this.state.conversations];
            let c = {
                ...conversations[index],
                closed: conversation.closed
            }
            conversations[index] = c;
            this.setState({conversations});
            this.setState({conversation});
        })
    }
    
    onSubmit = (data) => {
        this.setState({openModal: false})
        const newConversation = {
            createdBy: this.state.selectedUser || this.state.defaultUser,
            name: data.name,
            members: data.members
        }
         ConversationService.create(newConversation).then((res) => {
            const conversation = res.data;
            this.setState({conversations: [...this.state.conversations, conversation]});
        })
    }
}

export default RootContainer;