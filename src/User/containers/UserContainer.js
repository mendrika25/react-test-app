import react from 'react';
import UserList from '../components/UserList';
import UserService from '../services/UserService';

class UserContainer extends react.Component {

    render() {
        return (
            <div>
                <UserList users = {this.props.users} onUserSelect = {this.onUserSelect} hideDetail = {this.props.hideDetail}/>
            </div>
        )
    }

    onUserSelect = (user) => {
        this.props.onUserSelect(user);
    }
}

export default UserContainer;