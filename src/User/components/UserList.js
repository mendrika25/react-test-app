import react from 'react';
import './UserList.css';
import {browserHistory} from 'react-router';

class UserList extends react.Component {

    render() {
        return (
            <div>
                <span>Utilisateurs</span>
                <select onChange = {this.onUserSelect}>
                    {
                        this.props.users?.map((u, i) => (
                            <option key = {i} value={u._id}>{u.username}</option>
                        ))
                    }
                </select>
            </div>
        )
    }

    onUserSelect = (e) => {
        const user = this.props.users.filter(u => u._id === e.target.value)[0];
        this.props.onUserSelect(user);
        window.history.pushState('', '', '/');
        this.props.hideDetail(true);
    }
}

export default UserList;