import http from '../../http-common';

class UserService {
    
    getAll() {
        return http.get('/user');
    }

    getUserConversation(id) {
        return http.get(`/user/${id}`);
    }
}

export default new UserService();