import react from 'react';
import { Link } from 'react-router-dom';

class TableConversationList extends react.Component {

    render() {
        return (
            <table style={{width: '70%', height: '100%', textAlign: 'left', lineHeight: '40px'}} border="1">
                <thead>
                    <tr>
                        <th>Conversation</th>
                        <th>Créer par</th>
                        <th>Statut</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        this.props.conversations?.map((list, i) => (
                            <tr key={list._id}>
                                <td>
                                    <Link  to={!this.props.main ? `/conversation/${list._id}` : `/list/${list._id}`} onClick={ () => this.props.onLoadDetail(list._id)}>{list.name}
                                    </Link>
                                </td>
                                <td>{list.createdBy?.username}</td>
                                <td style={{color: list.closed ? 'red' : 'green'}}>{list.closed ? 'fermée' : 'ouverte'}</td>
                            </tr>
                        ))
                    }           
                </tbody>            
            </table>
        )
    }
}

export default TableConversationList;