import react from 'react';
import { Link } from 'react-router-dom';
import './ConversationForm.css';

class ConversationForm extends react.Component {

    members = [];
    name = '';
    valid = true;
    render() {
        return (
            <div className="form-container">
                <Link to="/" className="close-modal" onClick = {() => this.props.onCloseModal()}>X</Link>
                <div className="header-modal">
                    <h4>Nouvelle conversation</h4>
                </div>
                <div className="row">
                    <label>Nom*</label>
                    <input type="text" ref={el => this.val = el} onChange={this.handleChange}/>
                    
                </div>
                <div className="row">
                    <label>Membres</label>
                    {
                        this.props.users?.map((u,i) => (
                            <span key={i}><input type="checkbox" value = {u._id} onChange = {this.onMemberSelect.bind(this)}/> {u.username} </span>
                        ))
                    }
                </div>
                <div className="row">
                    <button onClick = {this.onSubmit}>Valider</button>
                </div>
            </div>
        )
    }

    onMemberSelect(e) {
        let index;   
        if (e.target.checked) {
        this.members?.push(e.target.value);
        
        } else {
        index = this.members?.indexOf(e.target.value);
        this.members?.splice(index, 1);
        }
       
    }

    onSubmit = () => {
        if (this.name === '') {
            this.valid = false;
        }else {
            this.props.onSubmit({name: this.name, members: this.members});
        }
        
    }

    handleChange = (evt) => {
        this.name = evt.target.value;
        
    }
}

export default ConversationForm;