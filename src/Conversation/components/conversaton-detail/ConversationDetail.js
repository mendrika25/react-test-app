import react from 'react';
import './ConversationDetail.css';
import { Link } from 'react-router-dom';

class ConversationDetail extends react.Component {

    render() {
        return (
            <div className="detail-container">              
                <div className="header">
                    <h4>{this.props.conversation?.name} </h4>
                    {
                        !this.props.main && 
                        <Link to="/" onClick={() => this.props.onCloseConversation(this.props.conversation._id)}>X</Link>
                    }    
                </div>
                            
                <div className="msg-list">
                    {
                        this.props.conversation?.messages?.map((msg,i) => (
                            <p key={i}>{msg.from.username}: {msg.message}</p>
                        ))
                    }
                </div>           
            </div>
        )
    }
}

export default ConversationDetail;