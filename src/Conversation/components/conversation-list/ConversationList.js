import react from 'react';
import { Link } from 'react-router-dom';

class ConversationList extends react.Component {

    render() {
        return (
            <div>
                {                
                    this.props.conversations?.map((list, i) => (
                        <p key={list._id}><Link  to={!this.props.main ? `/conversation/${list._id}` : `/list/${list._id}`} onClick={ () => this.props.onLoadDetail(list._id)}>{list.name}</Link></p>
                    ))
                }
            </div>
        )
    }
}

export default ConversationList;