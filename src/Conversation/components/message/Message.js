import react from 'react';
import './Message.css';

class Message extends react.Component {

    message = ''; 
    val = '';
    render() {
        return (
            <div className="msg-container">
                <textarea ref={el => this.val = el} onChange={this.handleChange} rows="5" cols="35" style={{resize: "none"}}  required={true}></textarea>
                <button style = {{marginTop: "30px"}} onClick = {this.onAddMessage}>Envoyer</button>
            </div>
        )
    }

    handleChange = (evt) => {
        this.message = evt.target.value;
        
    }

    onAddMessage = () => {
        this.props.onAddMessage(this.message);
        this.val.value = "";
        
    }
}

export default Message;