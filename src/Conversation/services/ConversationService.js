import http from '../../http-common';

class ConversationService {

    getAll() {
        return http.get('/conversation');
    }

    get(id) {
        return http.get(`/conversation/${id}`);
    }

    create(data) {
        return http.post('/conversation/new', data);
    }

    closeConversation(id) {
        return http.put(`/conversation/close/${id}`);
    }

    addMessage(id, data) {
        return http.put(`/conversation/addMessage/${id}`, data);
    }
}

export default new ConversationService();