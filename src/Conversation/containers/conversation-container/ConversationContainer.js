import react from 'react';
import ConversationDetail from '../../components/conversaton-detail/ConversationDetail';
import ConversationForm from '../../components/conversation-form/ConversationForm';
import ConversationList from '../../components/conversation-list/ConversationList';
import Message from '../../components/message/Message';
import './ConversationContainer.css';
import ConversationService from '../../services/ConversationService';

class ConversationContainer extends react.Component {

    constructor(props) {
        super(props);
        this.state = {
            conversation: {_id: null, messages: [], name: '', closed: false},
        }
    }

    render() {
        return(
            <div className="container" >
                <div >
                    <div className="content">
                        <div className="list">
                            <div className="list-content">
                                <ConversationList conversations = {this.props.conversations?.filter(c => c && !c.closed)} onLoadDetail = {this.onLoadDetail}/>
                            </div>                
                            <button onClick = {this.onCreateConversation}>créer une conversation</button>
                        </div>
                        <div className="right" style={{visibility: this.props.conversation?.closed || this.props.conversation === null ? "hidden" : "visible"}}>
                            <div className="conversation-detail">
                                <ConversationDetail conversation = {this.props.conversation} onCloseConversation={this.onCloseConversation}/>
                            </div>
                            <Message onAddMessage = {this.onAddMessage}/>
                        </div> 
                    </div>  
                </div>
            </div>
        )
    }

    onCreateConversation = () => {
        this.props.openModal(true);
    }

    onLoadDetail= (id) => {
        ConversationService.get(id).then((res) => {
            const conversation = res.data;
            this.setState({conversation: {_id: conversation._id, messages: conversation.messages, name: conversation.name, closed: conversation.closed}});
            this.props.onLoadDetail(this.state.conversation);
        });
    }

    onCloseConversation = (id) => {
        this.props.onCloseConversation(id);
    }

    onAddMessage = (msg) => {
        this.props.onAddMessage(msg);
    }
}

export default ConversationContainer;