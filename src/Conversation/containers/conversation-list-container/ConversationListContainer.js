import react from 'react';
import { Link } from 'react-router-dom';
import ConversationDetail from '../../components/conversaton-detail/ConversationDetail';
import TableConversationList from '../../components/table-conversation-list/TableConversationList';
import ConversationService from '../../services/ConversationService';

class ConversationListContainer extends react.Component {

    constructor(props) {
        super(props);
        this.state = {
            conversations: [{_id: null, name: '', messages: [], closed: false}],
            conversation: {_id: null, messages: [], name: '', closed: false}           
        }
        
    }

    
    render() {
        return (
            <div style={{width: '800px', display: 'flex', flexDirection: 'row'}}>
                <div style={{alignSelf:'start', width: '100%', display: 'flex', flexDirection: 'column', gap: '30px'}}>
                    <TableConversationList main = {true} onLoadDetail = {this.onLoadDetail} conversations = {this.state.conversations}/>
                    <Link to="/">Précédent</Link>   
                </div>                  
                <ConversationDetail main = {true} conversation = {this.state.conversation}/>  
            </div>
        )
    }

    async componentDidMount() {
        const res = await ConversationService.getAll();
        this.setState({conversations: res.data})
    }


    onLoadDetail= (id) => {
        ConversationService.get(id).then((res) => {
            const conversation = res.data;
            this.setState({conversation: {_id: conversation._id, messages: conversation.messages, name: conversation.name, closed: conversation.closed}});
        });
    }
}

export default ConversationListContainer;