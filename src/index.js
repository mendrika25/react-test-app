import React from "react"
import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ConversationListContainer from "./Conversation/containers/conversation-list-container/ConversationListContainer";
import RootContainer from "./Root/RootContainer";

const content =  
    (
        <Router>
            <Switch>
                <Route path="/" exact component={RootContainer} />
                <Route path="/conversation/:id" exact component={RootContainer}></Route>
                <Route path="/list" exact component={ConversationListContainer}></Route>
                <Route path="/list/:id" exact component={ConversationListContainer}></Route>
            </Switch>
        </Router>
    )

ReactDOM.render(content, document.getElementById("root"))